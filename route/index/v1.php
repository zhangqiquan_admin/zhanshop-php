<?php
// +----------------------------------------------------------------------
// | index/v1【系统生成】   [ 2025-01-13 17:11:39 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2011~2025 zhangqiquan All rights reserved.
// +----------------------------------------------------------------------
// | Author: zhangqiquan <768617998@qq.com>
// +----------------------------------------------------------------------
declare (strict_types=1);

use zhanshop\App;

App::route()->group("/file", function (){
      App::route()->rule("GET", "uploadToken", [\app\api\index\v1\controller\File::class, "uploadToken"]);
      App::route()->rule("GET", "uploadCallback", [\app\api\index\v1\controller\File::class, "uploadcallback"]);
      App::route()->rule("POST", "uploadcallbackResult", [\app\api\index\v1\controller\File::class, "uploadcallbackResult"]);
});

App::route()->group("/passport", function (){
      App::route()->rule("POST", "login", [\app\api\index\v1\controller\Passport::class, "login"]);
      App::route()->rule("POST", "regBind", [\app\api\index\v1\controller\Passport::class, "regBind"]);
      App::route()->rule("POST", "smscode", [\app\api\index\v1\controller\Passport::class, "smscode"])->middleware([\zhanshop\middleware\IpLimitAuth::class]);
});

App::route()->group("/trades", function (){
      App::route()->rule("POST", "payment", [\app\api\index\v1\controller\Trades::class, "payment"])->middleware([\app\middleware\UserAuth::class]);
      App::route()->rule("POST", "callback", [\app\api\index\v1\controller\Trades::class, "callback"])->extra(["app","pay_way"]);
});

App::route()->group("/user", function (){
      App::route()->rule("GET", "info", [\app\api\index\v1\controller\User::class, "getInfo"]);
      App::route()->rule("POST", "info", [\app\api\index\v1\controller\User::class, "postInfo"]);
      App::route()->rule("DELETE", "info", [\app\api\index\v1\controller\User::class, "deleteInfo"]);
})->middleware([\app\middleware\UserAuth::class]);

