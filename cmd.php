#!/usr/bin/env php
<?php
use zhanshop\App;
require __DIR__ . '/vendor/autoload.php';

$console = (new App(__DIR__))->console();

// 运行控制台层逻辑
$output = $console->run($argv);

// 程序结束
$console->end($output);