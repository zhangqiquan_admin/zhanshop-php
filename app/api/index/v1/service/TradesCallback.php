<?php
// +----------------------------------------------------------------------
// | zhanshop-php / TradesCallback.php    [ 2025/1/13 15:09 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2011~2025 zhangqiquan All rights reserved.
// +----------------------------------------------------------------------
// | Author: zhangqiquan <768617998@qq.com>
// +----------------------------------------------------------------------
declare (strict_types=1);

namespace app\api\index\v1\service;

use zhanshop\App;
use zhanshop\payment\alipay\Callback as AlipayCallback;
use zhanshop\payment\alipay\Refund as AliRefund;
use zhanshop\payment\weixin\Callback as WeixinCallback;
use zhanshop\payment\weixin\Refund as WxRefund;

class TradesCallback
{
    /**
     * 支付宝APP支付回调
     * @param string $app
     * @param array $param
     * @return string|null
     * @throws \Exception
     */
    public function appalipay(string $app, array $param)
    {
        $callback = new AlipayCallback();
        $config = App::database()->table('payment_alipay')->where(['appname' => $app])->find();
        if($config == false) App::error()->setError($app.'支付方式未配置', 417);
        $callback->setConfig('appid', $config['appid']);
        $callback->setConfig('privatekey', $config['privatekey']);
        $callback->setConfig('publickey', $config['publickey']);
        return $callback->pay($param, function () use ($app, $param, $config){
            try {
                $param['passback_params'] = json_decode($param['passback_params'], true);
                $data = [
                    'order_id' => $param['out_trade_no'],
                    'ptrade_id' => $param['trade_no'],
                    'pay_time' => strtotime($param['gmt_payment']),
                    'subject' => $param['subject'],
                    'total_amount' => $param['total_amount'],
                    'attach' => $param['passback_params'],
                    'pay_method' => 'appalipay',
                    'params' => $param
                ];
                $goodsType = $data['attach']['type'];
                // 去写入订单和其他相关操作
                ////////////////////////////////////////////////////////////
            }catch (\Throwable $e){
                if(strpos($e->getMessage(), "1062") === false){
                    $aliRefund = new AliRefund();
                    // 触发退款
                    $aliRefund->setConfig('appid', $config['appid']);
                    $aliRefund->setConfig('privatekey', $config['privatekey']);
                    $aliRefund->setConfig('publickey', $config['publickey']);
                    $aliRefund->pay($param['out_trade_no'], floatval($param['total_amount']));
                    App::error()->setError($e->getMessage());
                }
            }
        });
    }

    /**
     * 微信APP支付
     * @param string $app
     * @param array $param
     * @return string|null
     * @throws \Exception
     */
    public function appwxpay(string $app, array $param)
    {
        $callback = new WeixinCallback();
        $config = App::database()->table('payment_wxpay')->where(['appname' => $app])->find();
        if($config == false) App::error()->setError($app.'支付方式未配置', 417);
        $callback->setConfig('appid', $config['appid']);
        $callback->setConfig('mchid', $config['mchid']);
        $callback->setConfig('aeskey', $config['aeskey']);
        $callback->setConfig('serialnum', $config['serialnum']);
        $callback->setConfig('privatekey', $config['privatekey']);
        $callback->setConfig('publickey', $config['publickey']);
        return $callback->pay($param, function () use ($app, &$param, $config){
            try {
                $param['attach'] = json_decode($param['attach'], true);
                $data = [
                    'order_id' => $param['out_trade_no'],
                    'ptrade_id' => $param['transaction_id'],
                    'pay_time' => strtotime($param['success_time']),
                    'subject' => '',
                    'total_amount' => $param['amount']['total'] / 100,
                    'attach' => $param['attach'],
                    'pay_method' => 'appwxpay',
                    'params' => $param
                ];
                $goodsType = $data['attach']['type'];
                // 去写入订单和其他相关操作
                ////////////////////////////////////////////////////////////
            }catch (\Throwable $e){
                if(strpos($e->getMessage(), "1062") === false){
                    $wxRefund = new WxRefund();
                    // 触发退款
                    $wxRefund->setConfig('appid', $config['appid']);
                    $wxRefund->setConfig('mchid', $config['mchid']);
                    $wxRefund->setConfig('aeskey', $config['aeskey']);
                    $wxRefund->setConfig('serialnum', $config['serialnum']);
                    $wxRefund->setConfig('privatekey', $config['privatekey']);
                    $wxRefund->setConfig('publickey', $config['publickey']);
                    $wxRefund->pay($param['out_trade_no'], $param['amount']['total'] / 100);
                    App::error()->setError($e->getMessage());
                }
            }

        });
    }

    public function __call(string $name, array $arguments)
    {
        App::error()->setError('暂不支持'.$name.'支付方式方式', 417);
    }
}