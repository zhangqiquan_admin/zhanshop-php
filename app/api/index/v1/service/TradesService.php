<?php
// +----------------------------------------------------------------------
// | zhanshop-php / TradesService.php    [ 2025/1/13 14:13 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2011~2025 zhangqiquan All rights reserved.
// +----------------------------------------------------------------------
// | Author: zhangqiquan <768617998@qq.com>
// +----------------------------------------------------------------------
declare (strict_types=1);

namespace app\api\index\v1\service;

use app\constant\ApiCode;
use zhanshop\App;
use zhanshop\Helper;
use zhanshop\payment\alipay\Callback as AlipayCallback;
use zhanshop\payment\alipay\Pay as Alipay;
use zhanshop\payment\alipay\Refund as AliRefund;
use zhanshop\payment\weixin\Pay as Wxpay;

class TradesService
{
    /**
     * 调起支付
     * @param string $app
     * @param array $data
     * @param array $attach
     * @return array
     */
    public function payment(string $app, array $data, array $attach)
    {
        $orderId = Helper::orderId(rand(100000, 999999));
        $amount = 0.01;
        switch ($data['pay_way']){
            case "appalipay":
                $config = App::database()->table('payment_alipay')->where(['appname' => $app])->findOrFail();
                $alipay = new Alipay();
                $alipay->setConfig('appid', $config['appid']);
                $alipay->setConfig('privatekey', $config['privatekey']);
                $alipay->setConfig('publickey', $config['publickey']);
                $alipay->setConfig('notify', $config['notify']);
                $result = $alipay->app($orderId, $amount, $attach, ['subject' => '测试商品']);
                return [
                    'out_trade_no' => $orderId,
                    'trade_amount' => $amount,
                    'body' => $result,
                ];
                break;
            case "appwxpay":
                $config = App::database()->table('payment_wxpay')->where(['appname' => $app])->findOrFail();
                $wxpay = new Wxpay();
                $wxpay->setConfig('appid', $config['appid']);
                $wxpay->setConfig('mchid', $config['mchid']);
                $wxpay->setConfig('aeskey', $config['aeskey']);
                $wxpay->setConfig('serialnum', $config['serialnum']);
                $wxpay->setConfig('privatekey', $config['privatekey']);
                $wxpay->setConfig('publickey', $config['publickey']);
                $wxpay->setConfig('notifyurl', $config['notify']);
                $result = $wxpay->app($orderId, $amount, $attach);
                $result['out_trade_no'] = $orderId;
                $result['trade_amount'] = $amount;
                break;
            default:
                App::error()->setError("暂不支持".$data['pay_way'].'支付方式', ApiCode::NOT_FOUND);
        }
        return $result;
    }
    /**
     * 支付回调
     * @param string $app
     * @param array $data
     * @param array $attach
     * @return array
     */
    public function callback(string $app, string $payWay, array $param)
    {
        switch ($payWay){
            case "appalipay":




                break;
            case "appwxpay":
                break;
        }
    }
}