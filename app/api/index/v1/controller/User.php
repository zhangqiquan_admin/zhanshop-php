<?php
// +----------------------------------------------------------------------
// | zhanshop-php / User.php    [ 2025/1/13 11:24 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2011~2025 zhangqiquan All rights reserved.
// +----------------------------------------------------------------------
// | Author: zhangqiquan <768617998@qq.com>
// +----------------------------------------------------------------------
declare (strict_types=1);

namespace app\api\index\v1\controller;

use app\api\index\v1\service\UserService;
use app\library\BaseController;
use zhanshop\App;
use zhanshop\Request;
use zhanshop\Response;

class User extends BaseController
{
    /**
     * @api GET info 用户信息
     * @apiHeader string token 用户token
     * @apiMiddleware UserAuth::class
     * @apiGroup 用户模块
     * @apiDescription
     */
    public function getInfo(Request $request, Response $response)
    {
        $app = $this->getApp($request);
        $userId = $request->getData('user.user_id');
        $result = App::make(UserService::class)->getInfo($app, intval($userId));
        return $this->result($result);
    }
    /**
     * @api POST info 修改信息
     * @apiHeader string token 用户token
     * @apiParam string avatar 头像
     * @apiParam string nick 昵称
     * @apiParam int sex=0 性别，0保密，1男，2女
     * @apiMiddleware UserAuth::class
     * @apiGroup 用户模块
     * @apiDescription
     */
    public function postInfo(Request $request, Response $response)
    {


    }
    /**
     * @api DELETE info 注销用户
     * @apiHeader string token 用户token
     * @apiMiddleware UserAuth::class
     * @apiGroup 用户模块
     * @apiDescription
     */
    public function deleteInfo(Request $request, Response $response)
    {

    }
}