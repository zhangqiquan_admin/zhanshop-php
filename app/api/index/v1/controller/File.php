<?php
// +----------------------------------------------------------------------
// | zhanshop-php / File.php    [ 2025/1/13 13:33 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2011~2025 zhangqiquan All rights reserved.
// +----------------------------------------------------------------------
// | Author: zhangqiquan <768617998@qq.com>
// +----------------------------------------------------------------------
declare (strict_types=1);

namespace app\api\index\v1\controller;

use app\api\index\v1\service\FileService;
use app\library\BaseController;
use zhanshop\App;
use zhanshop\Request;
use zhanshop\Response;

class File extends BaseController
{
    /**
     * @api GET uploadToken 上传token
     * @apiParam string type 空间类型（可选值：img，file，audio，video）
     * @apiParam json policy 长传策略参数参考：https://developer.qiniu.com/kodo/manual/put-policy
     * @apiGroup 文档模块
     * @apiDescription
     */
    public function uploadToken(Request $request, Response $response)
    {
        $data = $request->validateRule([
            'type' => 'required',
            'policy' => 'json',
        ])->getData();
        $result = App::make(FileService::class)->uploadToken($data['type'], $data['policy'] ? $data['policy'] : null);
        return $this->result($result);
    }

    /**
     * @api GET uploadCallback 上传回调
     * @apiGroup 文档模块
     * @apiDescription
     */
    public function uploadcallback(Request $request, Response $response)
    {
        $param = $request->param();
        $result = App::make(FileService::class)->uploadcallback($param);
        return $this->result($result);
    }
    /**
     * @api POST uploadcallbackResult 回调查询
     * @apiParam string inputKey 上传时候的key
     * @apiGroup 文档模块
     * @apiDescription
     */
    public function uploadcallbackResult(Request $request, Response $response)
    {
        $data = $request->validateRule([
            'inputKey' => 'required',
        ])->getData();
        $result = App::make(FileService::class)->uploadcallbackResult($data['inputKey']);
        return $this->result($result);
    }
}