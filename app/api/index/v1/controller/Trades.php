<?php
// +----------------------------------------------------------------------
// | zhanshop-php / Trades.php    [ 2025/1/13 14:05 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2011~2025 zhangqiquan All rights reserved.
// +----------------------------------------------------------------------
// | Author: zhangqiquan <768617998@qq.com>
// +----------------------------------------------------------------------
declare (strict_types=1);

namespace app\api\index\v1\controller;

use app\api\index\v1\service\TradesCallback;
use app\api\index\v1\service\TradesService;
use app\library\BaseController;
use zhanshop\App;
use zhanshop\Request;
use zhanshop\Response;

class Trades extends BaseController
{
    /**
     * @api POST payment 支付调起
     * @apiParam string goods_type 商品类型（vip：会员套餐）
     * @apiParam string sku_id 货品id
     * @apiParam int quantity 下单数量
     * @apiParam string pay_way 支付方式（appalipay：支付宝APP支付，appwxpay：微信APP支付）
     * @apiMiddleware UserAuth::class
     * @apiGroup 交易模块
     * @apiDescription
     */
    public function payment(Request $request, Response $response)
    {
        $data = $request->validateRule([
            'goods_type' => 'required | string',
            'sku_id' => 'required | int',
            'quantity' => 'required | int',
            'pay_way' => 'required | string',
        ])->getData();
        $userId = $request->getData('user.user_id');
        $attach = [
            'type' => $data['goods_type'],
            'uid' => $userId,
            'sku' => $data['sku_id'],
            'buy' => $data['quantity'],
        ];
        $result = App::make(TradesService::class)->payment($this->getApp($request), $data, $attach);
        return $this->result($result);
    }

    /**
     * @api POST callback/{app}/{pay_way} 支付回调
     * @apiGroup 交易模块
     * @apiDescription
     */
    public function callback(Request $request, Response $response)
    {
        $app = $request->get("app");
        $payWay = $request->get("pay_way");
        $param = $request->param();
        $result = App::make(TradesCallback::class)->$payWay($app, $param);
        return $this->result($result);
    }
}