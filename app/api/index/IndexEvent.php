<?php
// +----------------------------------------------------------------------
// | zhanshop-proxy / IndexEvnet.php    [ 2023/12/4 21:17 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2011~2023 zhangqiquan All rights reserved.
// +----------------------------------------------------------------------
// | Author: zhangqiquan <768617998@qq.com>
// +----------------------------------------------------------------------
declare (strict_types=1);

namespace app\api\index;
use Swoole\Http\Request;
use Swoole\Http\Response;
use zhanshop\ServEvent;

class IndexEvent extends ServEvent {

    public string $servName = 'index';

    public function onWorkerStart($server, $workerId) :void{
        parent::onWorkerStart($server, $workerId);
    }
    /**
     * @param Request $request
     * @param Response $response
     * @return void
     */
    public function onRequest($request, $response) :void{
        parent::onRequest($request, $response);
    }
}