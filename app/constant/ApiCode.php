<?php

namespace app\constant;

class ApiCode
{
    const BAD_REQUEST = 400;
    const UNAUTHORIZED = 401;
    const FORBIDDEN = 403;
    const NOT_FOUND = 404;
    const METHOD_NOT_ALLOWED = 405;
    const NOT_ACCEPTABLE = 406;
    const EXPECTATION_FAILED = 417;
    const TOO_MANY_REQUEST = 429;


    // token缺失
    const TOKEN_MISSING = 10001;

    // token不合法
    const TOKEN_ILLEGAL = 10002;

    // token失效
    const TOKEN_EXPIRE = 10003;

    // token验证失败
    const TOKEN_FAILED = 10004;

    // 用户被禁止登陆
    const USER_FORBIDDEN = 10010;

    // 用户密码错误上限
    const USER_PWDERR_LIMIT = 10011;

    // 用户秘密错误
    const USER_PASSWORD_ERR = 10012;
}