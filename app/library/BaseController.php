<?php
// +----------------------------------------------------------------------
// | zhanshop-php / Controller.php    [ 2023/8/11 上午9:44 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2011~2024 zhangqiquan All rights reserved.
// +----------------------------------------------------------------------
// | Author: zhangqiquan <768617998@qq.com>
// +----------------------------------------------------------------------
declare (strict_types=1);

namespace app\library;

use zhanshop\Request;

class BaseController
{
    /**
     * @param Request $request
     * @return array|mixed|null
     */
    public function getApp(Request $request)
    {
        return $request->header("app-name", 'unknown');
    }
    public function result(mixed &$data = [], $msg = 'OK', $code = 0){
        return [
            'code' => $code,
            'msg' => $msg,
            'data' => $data,
        ];
    }
}