<?php

namespace app\provider;

use zhanshop\App;

class SmsProvider
{
    /**
     * 保存验证码
     * @param string $app
     * @param string $area
     * @param string $phone
     * @param mixed $code
     * @param int $validTime
     * @return void
     * @throws \RedisException
     */
    protected function setCode(string $app, string $area, string $phone, mixed $code, int $validTime = 300)
    {
        App::cache()->set($app.':smscode:'.$area.':'.$phone, $code, $validTime);
    }

    /**
     * 获取验证码
     * @param string $app
     * @param string $area
     * @param string $phone
     * @return false|mixed|\Redis|string
     * @throws \RedisException
     */
    public function getCode(string $app, string $area, string $phone)
    {
        return App::cache()->get($app.':smscode:'.$area.':'.$phone);
    }

    /**
     * 登录短信验证码发送
     * @param string $app
     * @param string $area
     * @param string $phone
     * @return void
     */
    public function login(string $app, string $area, string $phone)
    {
        $code = rand(100000, 999999);
        $this->setCode($app, $area, $phone, $code);
    }

    /**
     * 注册短信验证码发送
     * @param string $app
     * @param string $area
     * @param string $phone
     * @return void
     */
    public function register(string $app, string $area, string $phone)
    {
        $code = rand(100000, 999999);
        $this->setCode($app, $area, $phone, $code);
    }

    /**
     * 绑定短信验证码发送
     * @param string $app
     * @param string $area
     * @param string $phone
     * @return void
     */
    public function bind(string $app, string $area, string $phone)
    {
        $code = rand(100000, 999999);
        $this->setCode($app, $area, $phone, $code);
    }

    /**
     * 解绑短信验证码发送
     * @param string $app
     * @param string $area
     * @param string $phone
     * @return void
     */
    public function unbind(string $app, string $area, string $phone)
    {
        $code = rand(100000, 999999);
        $this->setCode($app, $area, $phone, $code);
    }

    public function __call(string $name, array $arguments)
    {
        App::error()->setError('暂不支持'.$name.'短信消息', 404);
    }
}