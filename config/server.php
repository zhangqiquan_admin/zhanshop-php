<?php
// +----------------------------------------------------------------------
// | zhanshop-docker-server / http.php    [ 2022/5/1 12:18 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2011~2022 zhangqiquan All rights reserved.
// +----------------------------------------------------------------------
// | Author: Administrator <768617998@qq.com>
// +----------------------------------------------------------------------

use zhanshop\ServEvent;
use zhanshop\App;
use app\api\index\IndexEvent;
use zhanshop\console\command\Server;

return [
    'servers' => [
        [
            'name'      => 'index',
            'mode'      => (int)App::env()->get("SERV_MODE", "1"),
            'host'      => "::",
            'port'      => (int)App::env()->get("SERV_PORT", "6201"),
            'sock_type' => 3,
            'serv_type' => Server::HTTP,
            'callbacks' => [
                ServEvent::ON_REQUEST => [IndexEvent::class, 'onRequest'],
            ]
        ],
    ],
    'settings' => [
        'enable_static_handler' => true,
        'http_autoindex' => true,
        'http_index_files' => ['index.html'],
        'document_root' => App::rootPath().DIRECTORY_SEPARATOR.'public',
        'send_yield' => true,
        // 一个TCP连接的最少需要占用 224 字节
        'max_connection' => 200000,
        // 底层默认分配2M(C)虚拟内存+8K(PHP)内存
        'max_coroutine' => 20000, // 一个协程初始化的时候默认约占用8K物理内存
        'buffer_output_size' => 2 * 1024 * 1024,
        'package_max_length' => 2 * 1024 * 1024,
        'open_tcp_keepalive' => true,
        'tcp_keepidle' => 60,
        'tcp_keepinterval' => 60,
        'tcp_keepcount' => 1,
    ],
    'crontab' => [
    ],
];
