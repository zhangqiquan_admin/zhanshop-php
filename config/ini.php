<?php
// +----------------------------------------------------------------------
// | flow-course / ini.php    [ 2021/10/29 9:46 上午 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2011~2021 zhangqiquan All rights reserved.
// +----------------------------------------------------------------------
// | Author: zhangqiquan <768617998@qq.com>
// +----------------------------------------------------------------------
declare (strict_types=1);

use zhanshop\App;

return [
    // 默认时区
    'date.timezone'            => App::env()->get('INI_TIMEZONE', 'Asia/Shanghai'),
    /**
     * 内存限制 比如这里限制1024M的内存 20万个连接约需要50M的初始内存，协程数量初始占用8K内存，每个进程就需要占用800M内存，如果开2个工作进程和一个任务跑满内存需要3个多G
     * // 一个TCP连接的最少需要占用 224 字节
     * 'max_connection' => 200000,
     * // 底层默认分配2M(C)虚拟内存+8K(PHP)内存
     * 'max_coroutine' => 20000, // 一个协程初始化的时候默认约占用8K物理内存
     */
    'memory_limit'             => App::env()->get('INI_MEMORY_LIMIT', '200M'),
    // 显示错误
    'display_errors'           => 'On',
    // 显示错误步骤
    'display_startup_errors'   => 'On',
    'max_execution_time'       => 0,
];