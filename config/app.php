<?php
// +----------------------------------------------------------------------
// | 应用设置1
// +----------------------------------------------------------------------
use zhanshop\App;

return [
    // App名称
    'app_name'      => App::env()->get('APP_NAME', 'zhanshop-server'),
    // App调式模式
    'app_debug'     => App::env()->get('APP_DEBUG', false),
    // APP公钥
    'app_key'       => App::env()->get('APP_KEY', 'zhanshop'),
    // APP签名密钥
    'app_secret'   => ['v1' => '068c01437878c3985abe18b490b1480a'],
    // APP注册列表
    'app_list'     => ['zhanshop'],
    // 序号用于集群服务的序号标识符号
    'serial_code'   => (int)App::env()->get('SERIAL_CODE', '0'),
    // 错误显示信息
    'error_message' => '系统错误！请稍后再试～',
    // 反向代理认证
    'rproxy_auth'   => App::env()->get('RPROXY_AUTH', "zhanshop"),
];
