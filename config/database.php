<?php

use zhanshop\App;

return [
    // 默认使用的数据库连接配置
    'default' => App::env()->get('DB_DEFAULT', 'mysql'),
    // 数据库连接配置信息
    'connections' => [
        'mysql' => [
            // 数据库类型
            'type' => 'mysql',
            // 服务器地址
            'hostname' => App::env()->get('DB_HOST', 'zhanshop-mysql'),
            // 数据库名
            'database' => App::env()->get('DB_DATABASE', 'zhanshop_php'),
            // 用户名
            'username' => App::env()->get('DB_USERNAME', 'root'),
            // 密码
            'password' => App::env()->get('DB_PASSWORD', '123456'),
            // 端口
            'hostport' => App::env()->get('DB_PORT', '3306'),
            // 数据库连接参数
            'params' => [
                PDO::ATTR_TIMEOUT => 5,
            ],
            // 数据库编码
            'charset' => App::env()->get('DB_CHARSET', 'utf8mb4'),
            //指定查询对象.
            'query' => 'query',
            'pool' => [
                'max_connections' => 30,
                'timeout' => 0.001,
            ],
        ]
    ],
];
