<?php

use zhanshop\App;

return [
    'host' => App::env()->get('ES_HOST', 'zhanshop-elasticsearch'),
    'port' => App::env()->get('ES_PORT', '9200'),
    'scheme' => App::env()->get('ES_SCHEME', 'http'),
    'user' => App::env()->get('ES_USER', 'elastic'),
    'pass' => App::env()->get('ES_PASS', 'cloud123456'),
];