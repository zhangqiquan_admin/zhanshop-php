-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2025-01-01 03:17:26
-- 服务器版本： 8.4.0
-- PHP 版本： 8.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `zhanshop_php`
--

-- --------------------------------------------------------

--
-- 表的结构 `clouds_volume`
--

CREATE TABLE `clouds_volume` (
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '数据卷名',
  `giturl` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT 'git地址',
  `gituser` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'git账号',
  `gitpwd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'git密码'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- 表的结构 `payment_alipay`
--

CREATE TABLE `payment_alipay` (
  `id` int NOT NULL COMMENT 'ID',
  `appname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'APP名',
  `appid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'APPID',
  `privatekey` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '私钥',
  `publickey` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公钥',
  `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '启用状态',
  `notify` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '支付通知地址',
  `quit_url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '网页支付跳回地址',
  `return_url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '完成按钮返回地址',
  `create_time` int UNSIGNED NOT NULL COMMENT '创建时间',
  `update_time` int UNSIGNED NOT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='支付方式-支付宝' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `payment_alipay`
--

INSERT INTO `payment_alipay` (`id`, `appname`, `appid`, `privatekey`, `publickey`, `is_enable`, `notify`, `quit_url`, `return_url`, `create_time`, `update_time`) VALUES
(1, 'zhanshop', '2012004136600000', 'MMMMMMMMMMMMMMMMMM', 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAg9BZz02nyeDNanYV4cDaPrGgropLAJTyGIs+91AuPRZhXjw/jYQAcluupBzhGirgZt/vtVBxV/4lHevw99yeWiP3RnL2s+4RGbmX9Raj6tOnZWF2U07JgEd2AkA5NioVzLb1fUx/MMMMMMMMMMM', 1, 'http://test-zhanshop.cn/v1/trades.callback/zhanshop', NULL, NULL, 1, 1);

-- --------------------------------------------------------

--
-- 表的结构 `payment_wxpay`
--

CREATE TABLE `payment_wxpay` (
  `id` int NOT NULL COMMENT 'ID',
  `appname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'APP名',
  `appid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'APPID',
  `mchid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商户号',
  `aeskey` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'API密钥',
  `serialnum` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'API证书编号',
  `privatekey` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '私钥',
  `publickey` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公钥',
  `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '启用状态',
  `create_time` int UNSIGNED NOT NULL COMMENT '创建时间',
  `update_time` int UNSIGNED NOT NULL COMMENT '更新时间',
  `notify` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '支付通知地址'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='支付方式-微信支付' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `payment_wxpay`
--

INSERT INTO `payment_wxpay` (`id`, `appname`, `appid`, `mchid`, `aeskey`, `serialnum`, `privatekey`, `publickey`, `is_enable`, `create_time`, `update_time`, `notify`) VALUES
(1, 'zhanshop', 'wx1000000000000', '1600000000', '0000000000000000000', '00000000000000000000000000000000000000', '-----BEGIN PRIVATE KEY-----\r\nMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\r\n-----END PRIVATE KEY-----', '-----BEGIN CERTIFICATE-----\r\nMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\r\n-----END CERTIFICATE-----', 1, 1, 1, 'http://test-zhanshop.cn/v1/trades.callback/zhanshop');

-- --------------------------------------------------------

--
-- 表的结构 `user_account`
--

CREATE TABLE `user_account` (
  `user_id` int UNSIGNED NOT NULL COMMENT '用户id',
  `app` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'APP',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '用户密码',
  `regip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '注册ip',
  `create_time` int UNSIGNED DEFAULT NULL COMMENT '注册时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户主账户' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `user_account`
--

INSERT INTO `user_account` (`user_id`, `app`, `user_name`, `password`, `regip`, `create_time`) VALUES
(1, 'zhanshop', '17602189021', NULL, '::ffff:127.0.0.1', 1736738472),
(5, 'zhanshop', '+8617602189021', NULL, '::ffff:127.0.0.1', 1738292975);

-- --------------------------------------------------------

--
-- 表的结构 `user_info`
--

CREATE TABLE `user_info` (
  `user_id` bigint NOT NULL COMMENT '用户id',
  `app` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'APP',
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT '头像',
  `nick` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '昵称',
  `birthday` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '生日',
  `sex` tinyint(1) DEFAULT '0' COMMENT '性别',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '手机号码',
  `regip` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '注册ip',
  `channel` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unknown' COMMENT '注册渠道',
  `vip_endtime` bigint UNSIGNED NOT NULL DEFAULT '0' COMMENT 'vip截止时间',
  `consume_balance` bigint UNSIGNED DEFAULT '0' COMMENT '充值消耗',
  `cancel_time` int UNSIGNED DEFAULT '0' COMMENT '注销申请时间',
  `create_time` int UNSIGNED NOT NULL COMMENT '注册时间',
  `update_time` int UNSIGNED NOT NULL COMMENT '更新时间',
  `exte_key` bigint DEFAULT NULL COMMENT '外部KEY'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户信息' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `user_info`
--

INSERT INTO `user_info` (`user_id`, `app`, `avatar`, `nick`, `birthday`, `sex`, `phone`, `regip`, `channel`, `vip_endtime`, `consume_balance`, `cancel_time`, `create_time`, `update_time`, `exte_key`) VALUES
(1, 'zhanshop', NULL, '', '', 0, '17602189021', '::ffff:127.0.0.1', 'xiaomi', 0, 0, 0, 1736738472, 1736738472, NULL),
(5, 'zhanshop', NULL, '', '', 0, '+8617602189021', '::ffff:127.0.0.1', 'xiaomi', 0, 0, 0, 1738292975, 1738292975, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `user_sns`
--

CREATE TABLE `user_sns` (
  `sns_id` bigint UNSIGNED NOT NULL COMMENT '组件ID',
  `app` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'APP',
  `open_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'openID',
  `sns_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '类型',
  `unionid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'unionid',
  `user_id` int UNSIGNED DEFAULT '0' COMMENT '用户id',
  `create_time` int UNSIGNED DEFAULT NULL COMMENT '创建时间',
  `update_time` int UNSIGNED DEFAULT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户第三方账户绑定关系' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `user_sns`
--

INSERT INTO `user_sns` (`sns_id`, `app`, `open_id`, `sns_type`, `unionid`, `user_id`, `create_time`, `update_time`) VALUES
(1, 'zhanshop', '17602189021', 'phone', '', 1, 1736738472, 1736738472),
(5, 'zhanshop', '+8617602189021', 'phone', '', 5, 1738292975, 1738292975);

--
-- 转储表的索引
--

--
-- 表的索引 `payment_alipay`
--
ALTER TABLE `payment_alipay`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `rpc_key` (`appname`) USING BTREE;

--
-- 表的索引 `payment_wxpay`
--
ALTER TABLE `payment_wxpay`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `rpc_key` (`appname`) USING BTREE;

--
-- 表的索引 `user_account`
--
ALTER TABLE `user_account`
  ADD PRIMARY KEY (`user_id`) USING BTREE,
  ADD UNIQUE KEY `app` (`app`,`user_name`);

--
-- 表的索引 `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`user_id`) USING BTREE;

--
-- 表的索引 `user_sns`
--
ALTER TABLE `user_sns`
  ADD PRIMARY KEY (`sns_id`) USING BTREE,
  ADD UNIQUE KEY `app` (`app`,`open_id`,`sns_type`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `payment_alipay`
--
ALTER TABLE `payment_alipay`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `payment_wxpay`
--
ALTER TABLE `payment_wxpay`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `user_account`
--
ALTER TABLE `user_account`
  MODIFY `user_id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户id', AUTO_INCREMENT=6;

--
-- 使用表AUTO_INCREMENT `user_sns`
--
ALTER TABLE `user_sns`
  MODIFY `sns_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '组件ID', AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
