<?php
// +----------------------------------------------------------------------
// | zhanshop-cloud / bootstrap.php    [ 2025/1/9 15:41 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2011~2025 zhangqiquan All rights reserved.
// +----------------------------------------------------------------------
// | Author: zhangqiquan <768617998@qq.com>
// +----------------------------------------------------------------------
declare (strict_types=1);

use zhanshop\App;

require dirname(dirname(dirname(__FILE__))).'/vendor/autoload.php';
$console = (new App(dirname(dirname(__DIR__))))->console();