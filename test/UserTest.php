<?php

namespace test;

use zhanshop\App;
use zhanshop\Httpclient;
use zhanshop\TestCase;

class UserTest extends TestCase
{
    public function testLogin()
    {

        $time = strval(time());
        $secretVer = 'v1';
        $url = "http://127.0.0.1:6201/v1/passport.login";
        $path = parse_url($url, PHP_URL_PATH);
        $appName = 'zhanshop';

        $secret = App::config()->get("app.app_secret")[$secretVer];

        $httpClient = new Httpclient();
        $data = json_encode([
            'channel' => 'xiaomi',
            'type' => 'phone',
            'data' => [
                'area' => '+86',
                'phone' => '14000000000',
                'code' => '336699',
            ],
        ]);
        $appSign = $secretVer.':'.base64_encode(hash_hmac("sha1", $appName.$time.$path.$data, $secret, true));
        $result = $httpClient->setTimeout(3000)->setHeader('app-name', $appName)->setHeader('app-time', $time)->setHeader("app-sign", $appSign)->setHeader('content-type', 'application/json')->request($url, "POST", $data);
        $body = json_decode($result['body'], true);
        var_dump($body);
    }

    public function testTextLogin()
    {
        $time = strval(time());
        $secretVer = 'v1';
        $url = "http://127.0.0.1:6201/v1/passport.login";
        $path = parse_url($url, PHP_URL_PATH);
        $appName = 'zhanshop';

        $secret = App::config()->get("app.app_secret")[$secretVer];

        $httpClient = new Httpclient();
        $data = http_build_query([
            'channel' => 'xiaomi',
            'type' => 'phone',
            'data' => [
                'area' => '+86',
                'phone' => '14000000000',
                'code' => '336699',
            ],
        ]);
        $appSign = $secretVer.':'.base64_encode(hash_hmac("sha1", $appName.$time.$path.$data, $secret, true));
        $result = $httpClient->setTimeout(3000)->setHeader('app-name', $appName)->setHeader('app-time', $time)->setHeader("app-sign", $appSign)->setHeader('content-type', 'application/x-www-form-urlencoded')->request($url, "POST", $data);
        $body = json_decode($result['body'], true);
        var_dump($body);
    }
}